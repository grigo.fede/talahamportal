import { Kingdoms, SettlementType } from './enums';

export type SettlementDataType = {
    id: string | number;
    type: SettlementType;
    name: string;
    kingdom: Kingdoms;
    position: number[];
    showInfo: boolean;
};

export const southSettlements: SettlementDataType[] = [
    {
        id: 1,
        name: 'Spirit Forest',
        position: [-116, 123.0625],
        kingdom: 3,
        showInfo: true,
        type: 1
    },
    {
        id: 2,
        name: 'Woodmont',
        position: [-154.1875, 129.1875],
        kingdom: 3,
        showInfo: true,
        type: 0
    },
    {
        id: 3,
        name: 'Oldforest',
        position: [-159.8125, 157.75],
        kingdom: 3,
        showInfo: true,
        type: 0
    },
    {
        id: 4,
        name: 'Spirit Garden',
        position: [-167.125, 114.25],
        kingdom: 3,
        showInfo: true,
        type: 0
    },
    {
        id: 4,
        name: 'Esconya',
        position: [-164, 100.125],
        kingdom: 3,
        showInfo: true,
        type: 0
    },
    {
        id: 5,
        name: 'Nubaton',
        position: [-152.5625, 90.125],
        kingdom: 3,
        showInfo: true,
        type: 0
    },
    {
        id: 6,
        name: 'Itianae',
        position: [-178.75, 53.6875],
        kingdom: 3,
        showInfo: true,
        type: 0
    },
    {
        id: 7,
        name: 'Rhodur',
        position: [-182.375, 68.5],
        kingdom: 3,
        showInfo: true,
        type: 0
    },
    {
        id: 8,
        name: 'Sunguad',
        position: [-186, 48.875],
        kingdom: 3,
        showInfo: true,
        type: 0
    }
];

export const westSettlements: SettlementDataType[] = [
    {
        id: 9,
        name: 'Silver Bush',
        position: [-98.67381562249221, 101.41248506989157],
        kingdom: 4,
        showInfo: true,
        type: 1
    },
    {
        id: 10,
        name: 'Corte di Karl',
        position: [-95.25, 61.75],
        kingdom: 4,
        showInfo: true,
        type: 0
    },
    {
        id: 11,
        name: 'Corte di Aron',
        position: [-118, 60.125],
        kingdom: 4,
        showInfo: true,
        type: 0
    },
    {
        id: 12,
        name: 'Corte di Ben',
        position: [-111.75, 48],
        kingdom: 4,
        showInfo: true,
        type: 0
    },
    {
        id: 13,
        name: 'Corte di Denise',
        position: [-128.5, 46.75],
        kingdom: 4,
        showInfo: true,
        type: 0
    },
    {
        id: 14,
        name: 'Corte di Nessuno',
        position: [-105.875, 36.0625],
        kingdom: 4,
        showInfo: true,
        type: 0
    },
    {
        id: 15,
        name: 'Città Ponte del Fiume',
        position: [-126.3125, 20.375],
        kingdom: 4,
        showInfo: true,
        type: 0
    },
    {
        id: 16,
        name: 'Oakview',
        position: [-85.6875, 14.25],
        kingdom: 4,
        showInfo: true,
        type: 0
    },
    {
        id: 17,
        name: 'Swanrun',
        position: [-75.1875, 24.8125],
        kingdom: 4,
        showInfo: true,
        type: 0
    },
    {
        id: 18,
        name: 'Applegrave',
        position: [-64.125, 26.125],
        kingdom: 4,
        showInfo: true,
        type: 0
    },
    {
        id: 19,
        name: 'Coldguard',
        position: [-53.4375, 6.1875],
        kingdom: 4,
        showInfo: true,
        type: 0
    }
];

export const eastSettlements: SettlementDataType[] = [
    {
        id: 20,
        name: 'Breaker Point',
        position: [-98.2, 147.125],
        kingdom: 2,
        showInfo: true,
        type: 1
    },
    {
        id: 21,
        name: 'Drina',
        position: [-112.7, 175],
        kingdom: 2,
        showInfo: true,
        type: 0
    },
    {
        id: 22,
        name: 'Boeren',
        position: [-92.525, 182.2875],
        kingdom: 2,
        showInfo: true,
        type: 0
    },
    {
        id: 23,
        name: 'Tribica',
        position: [-105.375, 186.875],
        kingdom: 2,
        showInfo: true,
        type: 0
    },
    {
        id: 24,
        name: 'Bizantias',
        position: [-98.3125, 166.3125],
        kingdom: 2,
        showInfo: true,
        type: 0
    },
    {
        id: 25,
        name: 'Butterwick',
        position: [-107.4025, 201.45],
        kingdom: 2,
        showInfo: true,
        type: 0
    },
    {
        id: 26,
        name: 'Millharbor',
        position: [-136.1625, 192.65],
        kingdom: 2,
        showInfo: true,
        type: 0
    },
    {
        id: 27,
        name: 'Firhorn',
        position: [-59.625, 216.0625],
        kingdom: 2,
        showInfo: true,
        type: 0
    },
    {
        id: 28,
        name: 'Herraren',
        position: [-74.3125, 223.8125],
        kingdom: 2,
        showInfo: true,
        type: 0
    },
    {
        id: 29,
        name: 'Runeshore',
        position: [-84.875, 225.5],
        kingdom: 2,
        showInfo: true,
        type: 0
    },
    {
        id: 30,
        name: 'Villaggio di Braun',
        position: [-65.9375, 244.6875],
        kingdom: 2,
        showInfo: true,
        type: 0
    }
];

export const northSettlements: SettlementDataType[] = [
    {
        id: 31,
        name: 'Iron Keep',
        position: [-79.4, 124],
        kingdom: 1,
        showInfo: true,
        type: 1
    },
    {
        id: 32,
        name: 'Copperhall',
        position: [-59.6125, 128.125],
        kingdom: 1,
        showInfo: true,
        type: 0
    },
    {
        id: 33,
        name: 'Steel City',
        position: [-43.3625, 105.5125],
        kingdom: 1,
        showInfo: true,
        type: 0
    },
    {
        id: 34,
        name: 'Golden Cabin',
        position: [-40.825, 136.5625],
        kingdom: 1,
        showInfo: true,
        type: 0
    },
    {
        id: 35,
        name: 'Silver Keep',
        position: [-50.45, 67.4375],
        kingdom: 1,
        showInfo: true,
        type: 0
    },
    {
        id: 36,
        name: 'Diamond Mansion',
        position: [-38.25, 188.375],
        kingdom: 1,
        showInfo: true,
        type: 0
    },
    {
        id: 37,
        name: 'Monastero degli Elementi',
        position: [-26.1375, 201.6625],
        kingdom: 1,
        showInfo: true,
        type: 0
    },
    {
        id: 38,
        name: 'Unknown',
        position: [-27.875, 46.03125],
        kingdom: 1,
        showInfo: false,
        type: 0
    },
    {
        id: 39,
        name: 'Mystery',
        position: [-9.125, 80.96875],
        kingdom: 1,
        showInfo: false,
        type: 0
    }
];

export const settlements: SettlementDataType[] = [
    {
        id: 0,
        type: SettlementType.CAPITAL,
        name: 'Adamantium',
        kingdom: Kingdoms.CAPITAL,
        position: [-98.65008441694287, 124.238329950118],
        showInfo: true
    },
    ...southSettlements,
    ...westSettlements,
    ...eastSettlements,
    ...northSettlements
];
// [ -98.65008441694287, 124.238329950118 ]
