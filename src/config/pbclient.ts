import PocketBase, {
    RecordFullListQueryParams,
    RecordQueryParams,
    Record,
    FileQueryParams,
    BaseAuthStore,
    RecordAuthResponse
} from 'pocketbase';

class CustomPBClient {
    pb: PocketBase;
    authCollection: string;

    constructor(authCollection = 'users', url = 'http://127.0.0.1:8090') {
        this.pb = new PocketBase(url);
        this.pb.autoCancellation(false);
        this.authCollection = authCollection;
    }

    // Auth
    getAuthStore(): BaseAuthStore {
        return this.pb.authStore;
    }

    // check if the user is authenticated
    isAuth(): boolean {
        return this.pb.authStore.isValid;
    }

    getAuth<T = Record>(
        identification: string,
        secret: string,
        options?: RecordQueryParams
    ): Promise<RecordAuthResponse<T>> {
        return this.pb
            .collection(this.authCollection)
            .authWithPassword(identification, secret, {}, options);
    }

    refreshAuth(options?: RecordQueryParams): Promise<RecordAuthResponse<Record>> {
        return this.pb.collection(this.authCollection).authRefresh({}, options);
    }

    // password reset
    requestPwdReset(email: string, options?: RecordQueryParams): Promise<boolean> {
        return this.pb.collection(this.authCollection).requestPasswordReset(email, {}, options);
    }

    confirmPwdReset(
        resetToken: string,
        newPassword: string,
        newPasswordConfirm: string,
        options?: RecordQueryParams
    ): Promise<boolean> {
        return this.pb
            .collection(this.authCollection)
            .confirmPasswordReset(resetToken, newPassword, newPasswordConfirm, {}, options);
    }

    // email verification
    sendVerification(email: string, options?: RecordQueryParams): Promise<boolean> {
        return this.pb.collection(this.authCollection).requestVerification(email, {}, options);
    }

    confirmVerification(token: string, options?: RecordQueryParams): Promise<boolean> {
        return this.pb.collection(this.authCollection).confirmVerification(token, {}, options);
    }

    // email change
    requestEmailChange(newEmail: string, options?: RecordQueryParams): Promise<boolean> {
        return this.pb.collection(this.authCollection).requestEmailChange(newEmail, {}, options);
    }

    confirmEmailChange(
        emailChangeToken: string,
        userPassword: string,
        options?: RecordQueryParams
    ): Promise<boolean> {
        return this.pb
            .collection(this.authCollection)
            .confirmEmailChange(emailChangeToken, userPassword, {}, options);
    }

    // admin Auth
    getAdminAuth(email: string, password: string, options?: RecordQueryParams): Promise<any> {
        return this.pb.admins.authWithPassword(email, password, {}, options);
    }

    refreshAdminAuth(options?: RecordQueryParams): Promise<any> {
        return this.pb.admins.authRefresh({}, options);
    }

    // Data
    getSimpleFullList<T = Record>(
        collection: string,
        options?: RecordFullListQueryParams
    ): Promise<T[]> {
        return this.pb.collection(collection).getFullList<T>({ batch: 1000, ...options });
    }

    getRecordById<T = Record>(
        collection: string,
        recordId: string,
        options?: RecordQueryParams
    ): Promise<T> {
        return this.pb.collection(collection).getOne(recordId, options);
    }

    createRecord<T = Record, R = Partial<T>>(
        collection: string,
        body: R,
        options?: RecordQueryParams
    ): Promise<T> {
        return this.pb.collection(collection).create<T>(body as any, options);
    }

    updateRecord<T = Record, R = Partial<T>>(
        collection: string,
        recordId: string,
        body: R,
        options?: RecordQueryParams
    ): Promise<T> {
        return this.pb.collection(collection).update<T>(recordId, body as any, options);
    }

    delete(collection: string, recordId: string, options?: RecordQueryParams): Promise<boolean> {
        return this.pb.collection(collection).delete(recordId, options);
    }

    async getFileUrl(record: any, filename: string, options?: FileQueryParams): Promise<string> {
        return this.pb.files.getUrl(record, filename, options);
    }
}

const PBClient = new CustomPBClient();

Object.freeze(PBClient);

export default PBClient;
