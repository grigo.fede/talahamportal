export enum Kingdoms {
    NONE,
    NORTH,
    EAST,
    SOUTH,
    WEST,
    CAPITAL
}

export enum SettlementType {
    TOWN,
    CITY,
    CAPITAL
}
