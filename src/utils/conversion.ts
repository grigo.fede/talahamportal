import { Kingdoms, SettlementType } from 'config/enums';

export function getKingdomString(key: Kingdoms): string {
    switch (key) {
        case Kingdoms.CAPITAL:
            return 'CAPITAL';
        case Kingdoms.EAST:
            return 'EAST';
        case Kingdoms.NORTH:
            return 'NORTH';
        case Kingdoms.SOUTH:
            return 'SOUTH';
        case Kingdoms.WEST:
            return 'WEST';
        case Kingdoms.NONE:
            return '';

        default:
            return '';
    }
}

export function getKeyFromKingdomString(str: string): Kingdoms {
    switch (str) {
        case 'CAPITAL':
            return Kingdoms.CAPITAL;
        case 'EAST':
            return Kingdoms.EAST;
        case 'NORTH':
            return Kingdoms.NORTH;
        case 'SOUTH':
            return Kingdoms.SOUTH;
        case 'WEST':
            return Kingdoms.WEST;
        case '':
            return Kingdoms.NONE;

        default:
            return Kingdoms.NONE;
    }
}

export function getSettlementTypeString(key: SettlementType): string {
    switch (key) {
        case SettlementType.CAPITAL:
            return 'CAPITAL';
        case SettlementType.CITY:
            return 'CITY';
        case SettlementType.TOWN:
            return 'TOWN';
        default:
            return '';
    }
}

export function getStringSettlementType(value: string): SettlementType {
    switch (value) {
        case 'CAPITAL':
            return SettlementType.CAPITAL;
        case 'CITY':
            return SettlementType.CITY;
        case 'TOWN':
            return SettlementType.TOWN;
        default:
            return SettlementType.TOWN;
    }
}

export function hexToRgb(hex: string) {
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function (m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result
        ? {
              r: parseInt(result[1], 16),
              g: parseInt(result[2], 16),
              b: parseInt(result[3], 16)
          }
        : null;
}

export function getDayBaseTimestamp() {
    const date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    return date;
}

export function hexToRgbTextString(hex: string) {
    const result = hexToRgb(hex);
    return result ? result.r + ' ' + result.g + ' ' + result.b : null;
}

export function rgbToHex(r: number, g: number, b: number) {
    return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}
