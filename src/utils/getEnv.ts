export function getEnv(variable: string, defaultValue: any): any {
    console.log(variable);
    const t =
        (window as any)._env_ && (window as any)._env_[variable]
            ? (window as any)._env_[variable]
            : import.meta.env[`REACT_APP_${variable}`]
            ? import.meta.env[`REACT_APP_${variable}`]
            : defaultValue || undefined;
    return t;
}
