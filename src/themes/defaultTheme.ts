import { AppTheme } from 'types/themes';

const defaultTheme: AppTheme = {
    lightBackgroud: '#ffffff',
    background: '#FAFAFA',
    darkBackground: '#F2F2F2',
    primary: '#4E3524',
    darkPrimary: '#000000',
    lightPrimary: '#393939',
    secondary: '#84F0C7',
    darkSecondary: '#cab475',
    lightSecondary: '#FFFF97',
    highlight: '#F6F930',
    disabled: '#CCCCCC',
    negative: '#A22033',
    positive: '#15A234',
    warning: '#EB8527'
};

export default defaultTheme;
