import { LatLngExpression } from 'leaflet';

export function getCityMarker(
    lat: number,
    lng: number
): LatLngExpression[] | LatLngExpression[][] | LatLngExpression[][][] {
    const distances = [
        [-2.2011843775077864, -4.287485069891574],
        [-2.2636843775077864, 4.337514930108426],
        [1.1738156224922136, 3.5875149301084264],
        [3.2988156224922136, -0.09998506989157363],
        [1.7363156224922136, -3.2874850698915736]
    ];

    return distances.map(v => [v[0] + lat, v[1] + lng]);
}
