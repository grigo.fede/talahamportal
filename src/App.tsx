import L, { CRS } from 'leaflet';
import { MapContainer } from 'react-leaflet';
import RasterLayer from 'components/RasterLayer';
import Settlements from 'components/mapElements/Settlements';
import { useWindowDimensions } from 'hooks/useWindowDimensions';
import LandMarks from 'components/mapElements/Landmarks';
import { DEFAULT_THEME, applyTheme } from 'themes';
import CustomMap from 'components/CustomMap';
import Slider from 'components/navigation/Slider';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { useEffect, useState } from 'react';
import 'config/pbclient';
import { BREAKPOINT, EXTERN_LINE_GROUP, InitTables } from 'retables';
// react router
import { BrowserRouter, useRoutes } from 'react-router-dom';
import BasicView from 'views/BasicView';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { lstat } from 'fs';
import PBClient from 'config/pbclient';
import Cookie from 'cookie-universal';
import { useAppDispatch } from 'store';
import { setRole, setUser } from 'store/slices/userSlice';
import useAuth from 'hooks/useAuth';

function MyComponent() {
    const [value, setValue] = useState('');

    useEffect(() => {
        console.log(value);
    }, [value]);

    return (
        <div className='bg-white'>
            <ReactQuill theme='snow' value={value} onChange={setValue} />
        </div>
    );
}

const client = new QueryClient();

function App() {
    // hooks
    const { refreshFromCookie } = useAuth();

    // effects
    useEffect(() => {
        refreshFromCookie();
    }, []);

    return (
        <div className=' w-full'>
            <QueryClientProvider client={client}>
                <BrowserRouter>
                    <InitTables
                        breakpoint={BREAKPOINT.SM}
                        //headerRenderer={DefaultHeaderCell}
                        //cellRenderer={DefaultTableCell}

                        baseHeaderClasses='font-bold bg-primary text-white'
                        // Apply custom classes based on the row index
                        baseRowClasses={index => {
                            let baseClasses = 'py-2 bg-[#c2a77c] ';
                            return index % 2 !== 0
                                ? baseClasses
                                : `${baseClasses} bg-[#a29d68] dark:bg-stripe`;
                        }}
                        cellRenderer={cell => {
                            return <div className=''> {cell.text} </div>;
                        }}
                        baseCellPadding={{
                            vertical: '10px',
                            horizontal: '10px'
                        }}
                        gridConfig={{
                            showVerticalLines: false,
                            showExternLines: EXTERN_LINE_GROUP.ALL,
                            showHorizontalLines: false
                        }}>
                        <BasicView />
                    </InitTables>
                </BrowserRouter>
            </QueryClientProvider>
        </div>
    );
}

function getCenter(values: number[][]) {
    const vl = values.length;
    const result = [0, 0];
    let j = vl - 1;
    let area = 0;

    for (let i = 0; i < vl; j = i++) {
        let p1 = values[i];
        let p2 = values[j];
        area += p1[0] * p2[1];
        area -= p1[1] * p2[0];
    }

    area /= 2;
    j = vl - 1;

    for (let i = 0; i < vl; j = i++) {
        let p1 = values[i];
        let p2 = values[j];
        let f = p1[0] * p2[1] - p1[1] * p2[0];
        result[0] += (p1[0] + p2[0]) * f;
        result[1] += (p1[1] + p2[1]) * f;
    }

    result[0] /= area * 6;
    result[1] /= area * 6;

    return result;
}

function getDistances(values: number[][]) {
    const center = getCenter(values);

    return values.map(v => {
        return [v[0] - center[0], v[1] - center[1]];
    });
}

export default App;
