import { useEffect, useRef, useState } from 'react';

export function useWindowDimensions() {
    const [dimensions, setDimensions] = useState({
        width: window.innerWidth,
        height: window.innerHeight,
        mobile: window.innerWidth <= 768
    });

    function callback() {
        const newVal = {
            width: window.innerWidth,
            height: window.innerHeight,
            mobile: window.innerWidth <= 768
        };

        setDimensions(newVal);
    }

    useEffect(() => {
        window.addEventListener('resize', callback);
        return () => {
            window.removeEventListener('resize', callback);
        };
    }, [window]);

    return dimensions;
}
