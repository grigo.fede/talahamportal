import PBClient from 'config/pbclient';
import Cookie from 'cookie-universal';
import { Record, RecordAuthResponse } from 'pocketbase';
import { useAppDispatch } from 'store';
import { setAvatar, setRole, setUser } from 'store/slices/userSlice';

function useAuth() {
    // hooks
    const dispatch = useAppDispatch();

    function updateUserHandler(res: RecordAuthResponse<Record>) {
        const cookies = Cookie();
        dispatch(
            setUser({
                user: {
                    id: res.record.id,
                    name: res.record.name,
                    surname: res.record.username,
                    email: res.record.email,
                    avatar: res.record.avatar,
                    username: res.record.username
                }
            })
        );
        dispatch(setRole({ role: res.record.role }));
        if (cookies.get('authcookie'))
            cookies.set('authcookie', PBClient.getAuthStore().exportToCookie());
        console.log('registriamo', res.record);
        if (res.record.avatar.length > 0) {
            PBClient.getFileUrl(res.record, res.record.avatar || '', {}).then(res => {
                console.log('fetched avatar', res);
                dispatch(setAvatar({ avatar: res }));
            });
        }
    }

    function refreshUser(handler?: (res: RecordAuthResponse<Record>) => void) {
        PBClient.refreshAuth()
            .then(res => {
                updateUserHandler(res);
                if (handler) handler(res);
            })
            .catch(err => console.log(err));
    }

    function refreshFromCookie(handler?: (res: RecordAuthResponse<Record>) => void) {
        const cookies = Cookie();
        if (cookies.get('authcookie')) {
            PBClient.getAuthStore().loadFromCookie(cookies.get('authcookie'));
            refreshUser(handler);
        }
    }

    return { refreshUser, refreshFromCookie, updateUserHandler };
}

export default useAuth;
