import { useQuery } from '@tanstack/react-query';
import PBClient from 'config/pbclient';
import { useMemo } from 'react';
import { InfoMarker } from 'types/responses/PbTypes';

function useInfoMarkers() {
    const { data: infoMarkersData } = useQuery(
        ['infoMarkers'],
        () => {
            return PBClient.getSimpleFullList<InfoMarker>('infoMarker', {
                expand: 'marker'
            });
        },
        {
            retry: 0,
            refetchInterval: 0
        }
    );

    const inforMarkers = useMemo(() => {
        if (infoMarkersData) {
            return infoMarkersData.map(data => {
                return {
                    position: [data.expand.marker.lat, data.expand.marker.lng],
                    zoomLv: data.expand.marker.zoomlv,
                    visible: data.expand.marker.visible,
                    title: data.title,
                    description: data.description,
                    infoEnable: data.expand.marker.infoEnable
                };
            });
        }
        return [];
    }, [infoMarkersData]);

    return { inforMarkers };
}

export default useInfoMarkers;
