import PBClient from 'config/pbclient';
import { SettlementDataType } from 'config/settlementList';
import { useCallback, useMemo } from 'react';
import { InfoMarker, SettlementItem, SettlementListResponse } from 'types/responses/PbTypes';
import { getKeyFromKingdomString, getStringSettlementType } from 'utils';

/*  This hook is used to fetch settlements data from the server.
    It returns a function that can be used to fetch settlements data.
    The function returns a promise that resolves to an array of SettlementDataType.
    The array is sorted by the settlement name.
    The SettlementDataType is defined in config/settlementList.ts.   
 */
function useSettlements() {
    const fetchSettlements = useCallback(async () => {
        const result = await PBClient.getSimpleFullList<SettlementItem>('settlementMarker', {
            expand: 'marker,png'
        });

        return result.map<SettlementDataType>(item => {
            return {
                id: item.id,
                name: item.name,
                position: [item.expand.marker.lat, item.expand.marker.lng],
                showInfo: item.expand.marker.infoEnable,
                kingdom: getKeyFromKingdomString(item.region),
                type: getStringSettlementType(item.settlementType)
            };
        });
    }, []);

    const fetchOne = useCallback(async (id: string) => {
        const result = await PBClient.getRecordById<SettlementItem>('settlementMarker', id, {
            expand: 'marker,png'
        });

        return result;
    }, []);

    return { fetchSettlements, fetchOne };
}

export default useSettlements;
