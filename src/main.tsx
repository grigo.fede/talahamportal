import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'themes/index.css';

// react redux
import store from 'store';
import { Provider } from 'react-redux';
import { applyTheme } from 'themes';

applyTheme('base');

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <div className='mainBackground h-full min-h-screen w-full'>
            <Provider store={store}>
                <App />
            </Provider>
        </div>
    </React.StrictMode>
);
