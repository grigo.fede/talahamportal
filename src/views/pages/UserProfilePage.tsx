import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import ProfileForm from 'components/forms/UserProfileForm';
import PBClient from 'config/pbclient';
import useAuth from 'hooks/useAuth';
import { Record } from 'pocketbase';
import { useEffect, useState } from 'react';
import { useLoaderData } from 'react-router-dom';
import { useAppSelector } from 'store';
import { UserRecord } from 'types/responses/PbTypes';

function ProfilePage() {
    // hooks
    const { refreshUser } = useAuth();

    // global state
    const { user } = useAppSelector(state => state.user);

    // state
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [avatar, setAvatar] = useState<File>();
    const [avatarUrl, setAvatarUrl] = useState<string>();

    // api
    const queryClient = useQueryClient();

    const { data: userData } = useQuery(
        ['userInfo'],
        () => {
            return PBClient.getRecordById<UserRecord>('users', user?.id || '', {});
        },
        {
            retry: 0,
            refetchInterval: 0
        }
    );

    const { mutateAsync } = useMutation<any, any, { reqData: UserRecord }, any>(
        ['avatarMutation'],
        (variables: { reqData: UserRecord }) => {
            return PBClient.getFileUrl(
                variables.reqData,
                // {
                //     id: userData?.id || '',
                //     collectionId: userData?.collectionId || '',
                //     collectionName: userData?.collectionName || ''
                // } as Record,
                variables.reqData?.avatar || '',
                {}
            );
        }
    );

    // effects
    useEffect(() => {
        if (userData) {
            setName(userData.name);
            setSurname(userData.surname);
            setUsername(userData.username);
            setEmail(userData.email);

            if (userData.avatar && userData.avatar.length === 0) setAvatarUrl(undefined);
            else
                mutateAsync({ reqData: userData })
                    .then(res => {
                        console.log(res, 'risultato ricerca avatar');
                        setAvatarUrl(res);
                    })
                    .catch(err => {
                        console.log("Couldn't get avatar url");
                        setAvatarUrl(undefined);
                        console.log(err);
                    });

            //setAvatarUrl(userData.avatar);
        }
    }, [userData]);

    return (
        <div className='px-8'>
            <ProfileForm
                email={email}
                name={name}
                avatar={avatarUrl}
                surname={surname}
                username={username}
                setName={setName}
                setSurname={setSurname}
                setEmail={setEmail}
                setUsername={setUsername}
                setAvatar={(v: File): void => {
                    setAvatar(v);
                    setAvatarUrl(URL.createObjectURL(v));
                }}
                submit={() => {
                    const data = new FormData();
                    data.append('name', name);
                    data.append('surname', surname);
                    data.append('username', username);
                    data.append('email', email);
                    if (avatar) data.append('avatar', avatar);
                    PBClient.updateRecord('users', user?.id || '', data, {})
                        .then(res => {
                            refreshUser();
                        })
                        .catch(err => {});
                }}
                cancel={() => {
                    queryClient.refetchQueries(['userInfo']);
                }}
            />
        </div>
    );
}

export default ProfilePage;
