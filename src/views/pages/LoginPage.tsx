import { useMutation, useQuery } from '@tanstack/react-query';
import LoginForm from 'components/LoginForm';
import PBClient from 'config/pbclient';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from 'store';
import { setAvatar, setRole, setUser } from 'store/slices/userSlice';
import { LoginResponse, UserRecord } from 'types/responses/PbTypes';
import Cookie from 'cookie-universal';
import useAuth from 'hooks/useAuth';

function LoginPage() {
    //hooks
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const { updateUserHandler } = useAuth();

    // state
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [rememberMe, setRememberMe] = useState(false);

    console.log(
        'login page',
        import.meta.env.VITE_DB_PASSWORD,
        import.meta.env.VITE_DEV_DATA,
        import.meta.env.MODE
    );

    // api
    const { mutate, mutateAsync } = useMutation<LoginResponse, {}, { user: string; pass: string }>(
        ['loginRequest'],
        (variables: { user: string; pass: string }) => {
            return PBClient.getAuth<UserRecord>(variables.user, variables.pass, {});
        },
        {
            retry: 0
        }
    );

    const { mutateAsync: fetchAvatar } = useMutation<any, any, { reqData: UserRecord }, any>(
        ['avatarMutation'],
        (variables: { reqData: UserRecord }) => {
            return PBClient.getFileUrl(variables.reqData, variables.reqData?.avatar || '', {});
        }
    );

    // callbacks

    const handleSubmit = async () => {
        mutateAsync({ user: email, pass: password })
            .then(res => {
                updateUserHandler(res as unknown as any);
                if (rememberMe) {
                    const cookies = Cookie();
                    cookies.set('authcookie', PBClient.getAuthStore().exportToCookie());
                }

                navigate('/');
            })
            .catch(err => {
                console.log(err);
            });
    };

    return (
        <LoginForm
            email={email}
            setEmail={setEmail}
            password={password}
            setPassword={setPassword}
            rememberMe={rememberMe}
            setRememberMe={setRememberMe}
            submit={handleSubmit}
        />
    );
}

export default LoginPage;
