import CustomMap from 'components/CustomMap';
import Slider from 'components/navigation/Slider';
import { useRoutes } from 'react-router-dom';
import MapPage from './MapPage';
import { Bars2Icon } from '@heroicons/react/20/solid';
import { Bars3Icon, UserIcon } from '@heroicons/react/24/outline';
import UserTopMenu from 'components/navigation/UserTopMenu';
import NavigationTopMenu from 'components/navigation/NavigationTopMenu';
import LoginPage from './pages/LoginPage';
import ProfilePage from './pages/UserProfilePage';
import PBClient from 'config/pbclient';
import { UserRecord } from 'types/responses/PbTypes';
import { useAppSelector } from 'store';
import { LockClosedIcon } from '@heroicons/react/24/solid';
import { use } from 'i18next';
import { useEffect, useMemo } from 'react';

function BasicView() {
    // global state

    const { user, avatar } = useAppSelector(state => state.user);

    // memos
    const isAuth: boolean = useMemo(() => !!user && PBClient.isAuth(), [user]);

    // routes
    const elements = useRoutes([
        {
            path: '/',
            element: <MapPage />
        },
        {
            path: '/signin',
            element: <LoginPage />
        },
        {
            path: '/profile',
            element: <ProfilePage />
            //loader: () => PBClient.getRecordById<UserRecord>('users', user?.id || '', {})
        }
    ]);

    // render
    return (
        <>
            <nav className={`sticky top-0 z-20 flex h-16 w-full justify-between p-2`}>
                <NavigationTopMenu>
                    <div className='mainBackground block h-12 w-12 cursor-pointer rounded-md border border-white hover:bg-slate-50/5'>
                        <Bars3Icon className='h-full w-full p-2.5 text-white' />
                    </div>
                </NavigationTopMenu>

                <UserTopMenu>
                    <div className='mainBackground relative block h-12 w-12 cursor-pointer rounded-md border border-white hover:bg-slate-50/5'>
                        {isAuth && avatar ? (
                            <img src={avatar} className='h-full w-full rounded-md' />
                        ) : (
                            <>
                                <UserIcon className='h-full w-full p-2.5 text-white' />
                                {!isAuth && (
                                    <LockClosedIcon className='absolute bottom-1.5 right-1.5 h-5 w-5 text-white' />
                                )}
                            </>
                        )}
                    </div>
                </UserTopMenu>
            </nav>
            {elements}
        </>
    );
}

export default BasicView;
