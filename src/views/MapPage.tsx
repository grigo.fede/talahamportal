import { MapPinIcon, PencilIcon } from '@heroicons/react/24/outline';
import CustomMap from 'components/CustomMap';
import Slider from 'components/navigation/Slider';
import Emitter from 'config/Emitter';
import PBClient from 'config/pbclient';
import { SettlementDataType } from 'config/settlementList';
import useSettlements from 'hooks/useSettlements';
import { useEffect, useState } from 'react';
import { Table } from 'retables';
import { useAppSelector } from 'store';

function MapPage() {
    // hooks
    const { fetchSettlements } = useSettlements();

    // global state

    const { role } = useAppSelector(state => state.user);

    // state
    const [settlement_items, setSettlementItems] = useState<SettlementDataType[]>([]);

    useEffect(() => {
        try {
            fetchSettlements()
                .then(d => setSettlementItems(d))
                .catch(e => console.error(e));
        } catch (error) {}
    }, []);

    return (
        <div className='p-8'>
            <CustomMap settlements={settlement_items} />

            <Table<SettlementDataType>
                className='mx-auto mt-10 max-w-5xl'
                indexKey='id'
                data={settlement_items}
                resizable={true}
                columnConfigs={[{ title: 'Luogo', key: 'name', flex: 0.3 }]}
                optionsCell={{
                    flex: 0.1,
                    renderer: item => {
                        return (
                            <div className='flex h-full w-full items-center justify-end space-x-2 pr-2'>
                                <div
                                    hidden={!!role && role !== 'player'}
                                    className=' flex h-10 w-10 cursor-pointer items-center justify-center rounded-full p-1.5 hover:bg-white/10'>
                                    <PencilIcon className='h-full w-full' />
                                </div>
                                <div
                                    className=' flex h-10 w-10 cursor-pointer items-center justify-center rounded-full p-1.5 hover:bg-white/10'
                                    onClick={e => {
                                        Emitter.emit('map:moveTo', {
                                            lat: item.item.position[0],
                                            lng: item.item.position[1],
                                            zoom: 4
                                        });
                                        window.scrollTo({
                                            top: 0,
                                            behavior: 'smooth'
                                        });
                                    }}>
                                    <MapPinIcon className='h-full w-full' />
                                </div>
                            </div>
                        );
                    }
                }}
            />

            <Slider />
        </div>
    );
}

export default MapPage;
