export interface SettlementListResponse {
    page: number;
    perPage: number;
    totalItems: number;
    totalPages: number;
    items: SettlementItem[];
}

export interface SettlementItem {
    collectionId: string;
    collectionName: string;
    created: Date;
    description: string;
    expand: {
        marker: Marker;
        png: Character[];
    };
    id: string;
    marker: string;
    name: string;
    picture: string;
    png: string[];
    region: string;
    settlementType: string;
    updated: Date;
}

export interface Marker {
    collectionId: string;
    collectionName: string;
    created: Date;
    id: string;
    infoEnable: boolean;
    lat: number;
    lng: number;
    updated: Date;
    visible: boolean;
    zoomlv: number;
}

export interface Character {
    collectionId: string;
    collectionName: string;
    created: Date;
    description: string;
    id: string;
    name: string;
    picture: string;
    updated: Date;
    visible: boolean;
}

export interface InfoMarker {
    collectionId: string;
    collectionName: string;
    created: Date;
    description: string;
    expand: {
        marker: Marker;
    };
    id: string;
    marker: string;
    title: string;
    updated: Date;
}

export interface UserRecord {
    avatar: string;
    collectionId: string;
    collectionName: string;
    created: Date;
    email: string;
    emailVisibility: boolean;
    id: string;
    name: string;
    surname: string;
    role: string;
    updated: Date;
    username: string;
    verified: boolean;
}

export interface LoginResponse {
    record: UserRecord;
    token: string;
}
