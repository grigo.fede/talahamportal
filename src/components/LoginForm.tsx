import { useState } from 'react';

function LoginForm(props: {
    email: string;
    setEmail: (email: string) => void;
    password: string;
    setPassword: (password: string) => void;
    rememberMe: boolean;
    setRememberMe: (rememberMe: boolean) => void;
    submit: () => void;
}) {
    const { email, setEmail, password, setPassword, rememberMe, setRememberMe } = props;

    return (
        <div className='flex min-h-full flex-1 items-center justify-center px-4 py-12 sm:px-6 lg:px-8'>
            <div className='w-full max-w-sm space-y-10'>
                <div>
                    <h2 className='mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-white'>
                        Accedi al tuo account
                    </h2>
                </div>
                <form
                    className='space-y-6'
                    onSubmit={e => {
                        e.preventDefault();
                        props.submit();
                    }}>
                    <div className='relative -space-y-px rounded-md shadow-sm'>
                        <div className='pointer-events-none absolute inset-0 z-10 rounded-md ring-1 ring-inset ring-gray-300' />
                        <div>
                            <label htmlFor='email-address' className='sr-only'>
                                Indirizzo email
                            </label>
                            <input
                                id='email-address'
                                name='email'
                                type='email'
                                autoComplete='email'
                                required
                                className='relative block w-full rounded-t-md border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-100 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6'
                                placeholder='Indirizzo email'
                                onChange={e => setEmail(e.target.value)}
                            />
                        </div>
                        <div>
                            <label htmlFor='password' className='sr-only'>
                                Password
                            </label>
                            <input
                                id='password'
                                name='password'
                                type='password'
                                autoComplete='current-password'
                                required
                                className='relative block w-full rounded-b-md border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-100 placeholder:text-gray-400 focus:z-10 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6'
                                placeholder='Password'
                                onChange={e => setPassword(e.target.value)}
                            />
                        </div>
                    </div>

                    <div className='flex items-center justify-between'>
                        <div className='flex items-center'>
                            <input
                                id='remember-me'
                                name='remember-me'
                                type='checkbox'
                                className='h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600'
                                checked={rememberMe}
                                onChange={e => setRememberMe(e.target.checked)}
                            />
                            <label
                                htmlFor='remember-me'
                                className='ml-3 block text-sm leading-6 text-white'>
                                Ricordami
                            </label>
                        </div>

                        <div className='text-sm leading-6'>
                            <a
                                href='#'
                                className='font-semibold text-indigo-600 hover:text-indigo-500'>
                                Forgot password?
                            </a>
                        </div>
                    </div>

                    <div>
                        <button
                            type='submit'
                            className='flex w-full justify-center rounded-md  border border-white px-3 py-1.5 text-sm font-semibold leading-6 text-white hover:bg-slate-50/5 '>
                            Sign in
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default LoginForm;
