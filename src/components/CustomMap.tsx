import { useWindowDimensions } from 'hooks/useWindowDimensions';
import { CRS, Map, control } from 'leaflet';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';
import RasterLayer from './RasterLayer';
import Settlements from './mapElements/Settlements';
import LandMarks from './mapElements/Landmarks';
import { RefObject, useEffect, useMemo, useRef } from 'react';
import { SettlementDataType } from 'config/settlementList';
import Emitter from 'config/Emitter';

function CustomMap(props: { settlements: SettlementDataType[] }) {
    const mapRef = useRef<Map>(null);

    const dimensions = useWindowDimensions();

    const dim = useMemo(() => {
        const result = dimensions.mobile ? dimensions.height - 64 : 770;
        return result;
    }, [dimensions]);

    useEffect(() => {
        Emitter.on('map:moveTo', handleMoveMapViewport);
        return () => {
            Emitter.off('map:moveTo', handleMoveMapViewport);
        };
    }, []);

    const handleMoveMapViewport = (e: { lat: number; lng: number; zoom: number }) => {
        mapRef.current?.setView([e.lat, e.lng], e.zoom, { animate: true, duration: 1.5 });
    };
    return (
        <>
            <MapContainer
                id={'map-container'}
                ref={mapRef}
                crs={CRS.Simple}
                style={{
                    height: dim,
                    maxWidth: 1035,
                    background: '#c2a77c',
                    marginLeft: 'auto',
                    marginRight: 'auto'
                }}
                className={'z-0 border-4'}>
                <RasterLayer minZoom={1.5} />
                <Settlements settlements={props.settlements} />
                <LandMarks />
                {/* <Marker
                position={[-98.3125, 166.3125]}
                icon={L.divIcon({
                    className: 'custom icon',
                    html: ReactDOM.renderToString(
                        <GiPositionMarker className='h-8 w-8 -translate-y-1/2 -translate-x-1/2 transform rounded-full transition-all duration-200 hover:scale-110' />
                    )
                })}>
                <Popup>Byzantias</Popup>
            </Marker> */}
            </MapContainer>
        </>
    );
}

export default CustomMap;
