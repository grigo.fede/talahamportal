import { useMap, useMapEvent } from 'react-leaflet';
import RasterCoords from 'leaflet-rastercoords';
import { RefObject, useEffect, useLayoutEffect, useRef } from 'react';
import L, { LeafletMouseEventHandlerFn, Map } from 'leaflet';
import { useWindowDimensions } from 'hooks/useWindowDimensions';

function RasterLayer(props: { minZoom: number }) {
    // const dimensions = useWindowDimensions();

    const alreadyDone = useRef(false);
    const _map = useMap();
    L.RasterCoords = RasterCoords as unknown as any;
    const _img = [4096, 3072];

    const onMapClick: LeafletMouseEventHandlerFn = e => {
        console.log(`[${e.latlng.lat}, ${e.latlng.lng}]`);
    };

    // useMapEvent('resize', map => {
    //     console.log('resize event');

    //     console.log(_map.getSize());
    //     setTimeout(() => _map.invalidateSize(), 400);
    // });

    // useEffect(() => {
    //     setTimeout(() => _map.invalidateSize(), 400);
    //     console.log('invalidated');
    // }, [dimensions.height, dimensions.mobile]);

    useLayoutEffect(() => {
        if (!alreadyDone.current) {
            var rc = new L.RasterCoords(_map, _img);
            _map.setMinZoom(props.minZoom);
            _map.setMaxZoom(5);
            // _map.setMaxZoom(rc.zoomLevel());
            _map.setView([-104.25, 123.25], props.minZoom, { animate: true });

            L.tileLayer(
                'https://github.com/FedericoGrigolini/talahamTyles/blob/main/{z}/{x}/{y}.png?raw=true',
                {
                    noWrap: true,
                    bounds: rc.getMaxBounds(),
                    maxNativeZoom: rc.zoomLevel()
                }
            ).addTo(_map);
            alreadyDone.current = true;
            _map.on('click', onMapClick);
            _map.on('resize', () => setTimeout(() => _map.invalidateSize(), 400));
        }
    }, []);
    return <></>;
}

export default RasterLayer;
