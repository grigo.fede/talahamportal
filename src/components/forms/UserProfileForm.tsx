import { PhotoIcon, UserCircleIcon } from '@heroicons/react/24/solid';
import { useRef } from 'react';

export default function ProfileForm(props: {
    name: string;
    setName: (v: string) => void;
    surname: string;
    setSurname: (v: string) => void;
    email: string;
    setEmail: (v: string) => void;
    username: string;
    setUsername: (v: string) => void;
    avatar?: string;
    setAvatar: (v: File) => void;
    submit: () => void;
    cancel: () => void;
}) {
    const inputRef = useRef<HTMLInputElement>(null);

    console.log(props.avatar);

    return (
        <form
            onSubmit={e => {
                e.preventDefault();
                props.submit();
            }}>
            <div className='space-y-12 sm:space-y-16'>
                <div>
                    <h2 className='text-base font-semibold leading-7 text-gray-50'>
                        Profilo Utente
                    </h2>

                    <div className='sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6 '>
                        <label
                            htmlFor='username'
                            className='block text-sm font-medium leading-6 text-gray-50 sm:pt-1.5'>
                            Username
                        </label>
                        <div className='mt-2 sm:col-span-2 sm:mt-0'>
                            <div className='flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md'>
                                <input
                                    type='text'
                                    name='username'
                                    id='username'
                                    autoComplete='username'
                                    className='block flex-1 border-0 bg-transparent py-2 px-2 text-gray-50 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6'
                                    placeholder='username'
                                    value={props.username}
                                    onChange={e => {
                                        props.setUsername(e.target.value);
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                    <div className='sm:grid sm:grid-cols-3 sm:items-center sm:gap-4 sm:py-6'>
                        <label
                            htmlFor='photo'
                            className='block text-sm font-medium leading-6 text-gray-50'>
                            Photo
                        </label>
                        <div className='mt-2 sm:col-span-2 sm:mt-0'>
                            <div className='flex items-center gap-x-3'>
                                {props.avatar ? (
                                    <img
                                        src={props.avatar}
                                        alt='avatar'
                                        className='h-12 w-12 rounded-full'
                                    />
                                ) : (
                                    <UserCircleIcon
                                        className='h-12 w-12 text-gray-300'
                                        aria-hidden='true'
                                    />
                                )}
                                <button
                                    type='button'
                                    onClick={() => {
                                        inputRef.current?.click();
                                    }}
                                    className='rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900  shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50'>
                                    Change
                                </button>
                                <input
                                    ref={inputRef}
                                    id='avatar-picker'
                                    type='file'
                                    className='hidden'
                                    name='avatar'
                                    onChange={e => {
                                        console.log(e.target.files);
                                        props.setAvatar(e.target.files![0]);
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                    <div className='mt-10 space-y-8 border-b border-gray-50/10 pb-12 sm:space-y-0 sm:divide-y sm:divide-gray-50/10 sm:border-t sm:pb-0'>
                        <div className='sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6'>
                            <label
                                htmlFor='first-name'
                                className='block text-sm font-medium leading-6 text-gray-50 sm:pt-1.5'>
                                First name
                            </label>
                            <div className='mt-2 rounded-md shadow-sm ring-1 ring-inset ring-gray-300 sm:col-span-2 sm:mt-0 sm:max-w-md'>
                                <input
                                    type='text'
                                    name='first-name'
                                    id='first-name'
                                    autoComplete='given-name'
                                    className='block flex-1 border-0 bg-transparent py-2 px-2 text-gray-50 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6'
                                    value={props.name}
                                    onChange={e => {
                                        props.setName(e.target.value);
                                    }}
                                />
                            </div>
                        </div>

                        <div className='sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6'>
                            <label
                                htmlFor='last-name'
                                className='block text-sm font-medium leading-6 text-gray-50 sm:pt-1.5'>
                                Last name
                            </label>
                            <div className='mt-2 rounded-md shadow-sm ring-1 ring-inset ring-gray-300 sm:col-span-2 sm:mt-0 sm:max-w-md'>
                                <input
                                    type='text'
                                    name='last-name'
                                    id='last-name'
                                    autoComplete='family-name'
                                    className='block flex-1 border-0 bg-transparent py-2 px-2 text-gray-50 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6'
                                    value={props.surname}
                                    onChange={e => {
                                        props.setSurname(e.target.value);
                                    }}
                                />
                            </div>
                        </div>

                        <div className='sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:py-6 '>
                            <label
                                htmlFor='email'
                                className='block text-sm font-medium leading-6 text-gray-50 sm:pt-1.5'>
                                Email address
                            </label>
                            <div className='mt-2 rounded-md shadow-sm ring-1 ring-inset ring-gray-300 sm:col-span-2 sm:mt-0 sm:max-w-md'>
                                <input
                                    id='email'
                                    name='email'
                                    type='email'
                                    autoComplete='email'
                                    className='block flex-1 border-0 bg-transparent py-2 px-2 text-gray-50 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6'
                                    value={props.email}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className='mt-6 flex items-center justify-end gap-x-6'>
                <button
                    type='button'
                    className='text-sm font-semibold leading-6 text-gray-50'
                    onClick={() => {
                        props.cancel();
                    }}>
                    Cancel
                </button>
                <button
                    type='submit'
                    className='inline-flex justify-center rounded-md bg-secondary px-3 py-2 text-sm font-semibold text-primary shadow-sm hover:bg-darkSecondary focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600'>
                    Save
                </button>
            </div>
        </form>
    );
}
