import L, { DivOverlay, LatLngExpression, Popup as LeafletPopup, PopupEvent } from 'leaflet';
import { useEffect, useMemo, useRef } from 'react';
import { Polygon, PopupProps } from 'react-leaflet';
import { BaseMapPolygon, getKingdomColor } from './mapUtils';
import {
    LeafletContextInterface,
    LeafletElement,
    createElementObject,
    createOverlayComponent,
    type SetOpenFunc,
    createElementHook,
    createDivOverlayComponent
} from '@react-leaflet/core';
import Emitter from 'config/Emitter';

export function getTownMarkerPaths(
    lat: number,
    lng: number
): LatLngExpression[] | LatLngExpression[][] | LatLngExpression[][][] {
    const distances = [
        [2.634505182379911, -0.0523387150355461],
        [0.07200518237991105, -1.739838715035546],
        [-1.677994817620089, -1.239838715035546],
        [-1.740494817620089, 1.385161284964454],
        [0.25950518237991105, 1.635161284964454]
    ];
    return distances.map(v => [v[0] + lat, v[1] + lng]);
}

function TownMarker(props: BaseMapPolygon) {
    const ref = useRef<any>(null);
    const paths = useMemo(() => {
        return getTownMarkerPaths(props.centerPoint[0], props.centerPoint[1]);
    }, [props.centerPoint]);

    const handleEventPopup = (e: { lat: number; lng: number; zoom: number }) => {
        if (e.lat === props.centerPoint[0] && e.lng === props.centerPoint[1]) {
            ref.current.openPopup();
        }
    };

    useEffect(() => {
        Emitter.on('map:moveTo', handleEventPopup);
        return () => {
            Emitter.off('map:moveTo', handleEventPopup);
        };
    }, []);

    return (
        <Polygon
            ref={ref}
            positions={paths}
            interactive={true}
            pathOptions={{
                color: props.showColor
                    ? props.kingdom
                        ? getKingdomColor(props.kingdom)
                        : 'yellow'
                    : 'transparent'
            }}
            className={'cursor-pointer'}>
            {/* <CustomPopup>ciaone</CustomPopup> */}
            {props.children}
        </Polygon>
    );
}

const CustomPopup = createOverlayComponent<any, PopupProps>(
    function createPopup(props, context) {
        const Pop = (p: typeof props) => {
            return <div>qualcosa</div>;
        };
        const popup = new LeafletPopup(props, context.overlayContainer);
        return createElementObject(popup, context);
    },
    function usePopupLifecycle(
        element: LeafletElement<LeafletPopup>,
        context: LeafletContextInterface,
        { position }: PopupProps,
        setOpen: SetOpenFunc
    ) {
        useEffect(
            function addPopup() {
                const { instance } = element;

                function onPopupOpen(event: PopupEvent) {
                    if (event.popup === instance) {
                        instance.update();
                        setOpen(true);
                    }
                }

                function onPopupClose(event: PopupEvent) {
                    if (event.popup === instance) {
                        setOpen(false);
                    }
                }

                context.map.on({
                    popupopen: onPopupOpen,
                    popupclose: onPopupClose
                });

                if (context.overlayContainer == null) {
                    // Attach to a Map
                    if (position != null) {
                        instance.setLatLng(position);
                    }
                    instance.openOn(context.map);
                } else {
                    // Attach to container component
                    context.overlayContainer.bindPopup(instance);
                }

                return function removePopup() {
                    context.map.off({
                        popupopen: onPopupOpen,
                        popupclose: onPopupClose
                    });
                    context.overlayContainer?.unbindPopup();
                    context.map.removeLayer(instance);
                };
            },
            [element, context, setOpen, position]
        );
    }
);

export default TownMarker;
