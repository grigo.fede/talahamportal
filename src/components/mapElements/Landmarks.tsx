import { Popup } from 'react-leaflet';
import CustomMarker from './CustomMarker';
import { LatLngTuple } from 'leaflet';
import useInfoMarkers from 'hooks/useInfoMarkers';

export default function LandMarks() {
    const { inforMarkers } = useInfoMarkers();

    return (
        <>
            {inforMarkers
                .filter(v => v.visible)
                .map((item, index) => {
                    return (
                        <CustomMarker
                            key={'info' + index}
                            hideUnder={item.zoomLv}
                            position={item.position as LatLngTuple}>
                            <Popup>{item.title}</Popup>
                        </CustomMarker>
                    );
                })}

            {/* <CustomMarker hideUnder={4} position={[-99.1875, 158.375]}>
                <Popup>Massacro sulla strada</Popup>
            </CustomMarker>
            <CustomMarker hideUnder={4} position={[-94.3125, 170.25]}>
                <Popup>Casa del contadino terrorizzata</Popup>
            </CustomMarker> */}
        </>
    );
}
