import { Polygon, Popup } from 'react-leaflet';
import TownMarker from './TownMarker';
import CityMarker from './CityMarker';
import { Kingdoms, SettlementType } from 'config/enums';
import AdamantiumMarker from './AdamantiumMarker';
import { useMemo } from 'react';
import { SettlementDataType } from 'config/settlementList';
import scrollImage from 'assets/inkarnate scroll.png';
import Emitter from 'config/Emitter';

export default function Settlements(props: { settlements: SettlementDataType[] }) {
    const elements = useMemo(() => {
        return props.settlements.map(val => {
            if (val.type === SettlementType.CAPITAL) {
                return (
                    <AdamantiumMarker
                        key='capital'
                        kingdom={val.kingdom}
                        centerPoint={val.position}>
                        <Popup>
                            <div className='font-pirata'>
                                <p className=' text-2xl tracking-wide'>{val.name}</p>
                                <p
                                    className='cursor-pointer text-center text-lg tracking-wide text-blue-500 underline'
                                    onClick={() => {
                                        Emitter.emit('openCitySlider', val.id);
                                    }}>
                                    {'Scopri'}
                                </p>
                            </div>
                        </Popup>
                    </AdamantiumMarker>
                );
            }
            if (val.type === SettlementType.CITY) {
                return (
                    <CityMarker key={val.name} kingdom={val.kingdom} centerPoint={val.position}>
                        <Popup>
                            <div className='font-pirata'>
                                <p className=' text-2xl tracking-wide'>{val.name}</p>
                                <p
                                    className='cursor-pointer text-center text-lg tracking-wide text-blue-500 underline'
                                    onClick={() => {
                                        Emitter.emit('openCitySlider', val.id);
                                    }}>
                                    {'Scopri'}
                                </p>
                            </div>
                        </Popup>
                    </CityMarker>
                );
            }
            if (val.type === SettlementType.TOWN) {
                return (
                    <TownMarker key={val.name} kingdom={val.kingdom} centerPoint={val.position}>
                        <Popup className={'request-popup'}>
                            <div className='font-pirata'>
                                <p className=' text-2xl tracking-wide'>{val.name}</p>
                                <p
                                    className='cursor-pointer text-center text-lg tracking-wide text-blue-500 underline'
                                    onClick={() => {
                                        Emitter.emit('openCitySlider', val.id);
                                    }}>
                                    {'Scopri'}
                                </p>
                            </div>
                        </Popup>
                    </TownMarker>
                );
            }
            return undefined;
        });
    }, [props.settlements]);

    return <>{elements.map(Element => Element)}</>;
}
