import { LatLngExpression } from 'leaflet';
import { useEffect, useMemo, useRef } from 'react';
import { Polygon, useMap } from 'react-leaflet';
import { BaseMapPolygon, getKingdomColor } from './mapUtils';
import Emitter from 'config/Emitter';

export function getCityMarkerPaths(
    lat: number,
    lng: number
): LatLngExpression[] | LatLngExpression[][] | LatLngExpression[][][] {
    const distances = [
        [-2.2011843775077864, -4.287485069891574],
        [-2.2636843775077864, 4.337514930108426],
        [1.1738156224922136, 3.5875149301084264],
        [3.2988156224922136, -0.09998506989157363],
        [1.7363156224922136, -3.2874850698915736]
    ];

    return distances.map(v => [v[0] + lat, v[1] + lng]);
}

function CityMarker(props: BaseMapPolygon) {
    const ref = useRef<any>(null);
    const paths = useMemo(() => {
        return getCityMarkerPaths(props.centerPoint[0], props.centerPoint[1]);
    }, [props.centerPoint]);

    const handleEventPopup = (e: { lat: number; lng: number; zoom: number }) => {
        if (e.lat === props.centerPoint[0] && e.lng === props.centerPoint[1]) {
            ref.current.openPopup();
        }
    };

    useEffect(() => {
        Emitter.on('map:moveTo', handleEventPopup);
        return () => {
            Emitter.off('map:moveTo', handleEventPopup);
        };
    }, []);

    return (
        <Polygon
            ref={ref}
            positions={paths}
            interactive={true}
            pathOptions={{
                color: props.showColor
                    ? props.kingdom
                        ? getKingdomColor(props.kingdom)
                        : 'yellow'
                    : 'transparent'
            }}
            className={'cursor-pointer border-yellow-300 text-yellow-400'}>
            {props.children}
        </Polygon>
    );
}

export default CityMarker;
