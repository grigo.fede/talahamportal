import { Marker, MarkerProps, Popup, useMap, useMapEvents } from 'react-leaflet';
import L from 'leaflet';
import { useMemo, useState } from 'react';

type CustomMarkerProps = { hideUnder?: number; hideOver?: number } & MarkerProps;

const MarkerIcon = new L.Icon({
    iconUrl: 'assets/mapmarker.webp',
    iconAnchor: undefined,
    shadowUrl: undefined,
    shadowSize: undefined,
    shadowAnchor: undefined,
    iconSize: new L.Point(45, 45),
    //className: 'leaflet-div-icon'
    className: 'animate-all cursor-pointer duration-300 hover:scale-110'
});

export default function CustomMarker(props: CustomMarkerProps) {
    const parentMap = useMap();
    const [zoomLevel, setZoomLevel] = useState(2);

    const show = useMemo(() => {
        if (props.hideOver !== undefined && props.hideOver < zoomLevel) {
            return false;
        }
        if (props.hideUnder !== undefined && props.hideUnder > zoomLevel) {
            return false;
        }
        return true;
    }, [zoomLevel]);

    useMapEvents({
        zoom: e => {
            setZoomLevel(parentMap.getZoom());
        }
    });

    return show ? (
        <Marker position={props.position} riseOnHover={true} icon={MarkerIcon}>
            {props.children}
        </Marker>
    ) : (
        <></>
    );
}
