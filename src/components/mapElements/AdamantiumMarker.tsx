import { LatLngExpression } from 'leaflet';
import { Polygon, Popup } from 'react-leaflet';
import { BaseMapPolygon, getKingdomColor } from './mapUtils';
import { useEffect, useMemo, useRef } from 'react';
import Emitter from 'config/Emitter';

export function getCapitalMarker(
    lat: number,
    lng: number
): LatLngExpression[] | LatLngExpression[][] | LatLngExpression[][][] {
    const distances = [
        [6.2125844169428746, 2.0741700498819995],
        [4.1500844169428746, 3.8241700498819995],
        [0.9625844169428746, 6.3866700498819995],
        [-3.3499155830571254, 7.1366700498819995],
        [-3.7874155830571254, 0.5741700498819995],
        [-4.0999155830571254, -7.4258299501180005],
        [0.025084416942874554, -6.4883299501180005],
        [3.9625844169428746, -4.0508299501180005],
        [3.9625844169428746, -0.3008299501180005]
    ];
    return distances.map(v => [v[0] + lat, v[1] + lng]);
}

export default function AdamantiumMarker(props: BaseMapPolygon) {
    const ref = useRef<any>(null);
    const paths = useMemo(() => {
        return getCapitalMarker(props.centerPoint[0], props.centerPoint[1]);
    }, [props.centerPoint]);

    const handleEventPopup = (e: { lat: number; lng: number; zoom: number }) => {
        if (e.lat === props.centerPoint[0] && e.lng === props.centerPoint[1]) {
            ref.current.openPopup();
        }
    };

    useEffect(() => {
        Emitter.on('map:moveTo', handleEventPopup);
        return () => {
            Emitter.off('map:moveTo', handleEventPopup);
        };
    }, []);

    return (
        <Polygon
            ref={ref}
            interactive={true}
            pathOptions={{
                color: props.showColor
                    ? props.kingdom
                        ? getKingdomColor(props.kingdom)
                        : 'blue'
                    : 'transparent'
            }}
            className={'cursor-pointer'}
            positions={paths}
            eventHandlers={{ mouseover: p => {} }}>
            {props.children}
        </Polygon>
    );
}
