import { Kingdoms } from 'config/enums';
import { PropsWithChildren } from 'react';

export type BaseMapElement = { hideUnder?: number; hideOver?: number };

export type BaseMapPolygon = BaseMapElement & {
    centerPoint: number[];
    kingdom?: Kingdoms;
    showColor?: boolean;
} & PropsWithChildren;

export function getKingdomColor(val: Kingdoms) {
    switch (val) {
        case Kingdoms.CAPITAL:
            return 'blue';
        case Kingdoms.WEST:
            return 'yellow';
        case Kingdoms.SOUTH:
            return 'green';
        case Kingdoms.EAST:
            return 'red';
        case Kingdoms.NORTH:
            return 'white';

        default:
            break;
    }
}
