import { Fragment, useEffect, useState } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import { HeartIcon, XMarkIcon } from '@heroicons/react/24/outline';
import { PencilIcon, PlusIcon } from '@heroicons/react/20/solid';
import Emitter from 'config/Emitter';
import { SettlementDataType, settlements } from 'config/settlementList';
import { Kingdoms, SettlementType } from 'config/enums';
import { SettlementItem } from 'types/responses/PbTypes';
import useSettlements from 'hooks/useSettlements';
import { getKeyFromKingdomString, getSettlementTypeString, getStringSettlementType } from 'utils';
import dompurify from 'dompurify';

function settlementTypeToString(id: SettlementType) {
    switch (id) {
        case SettlementType.TOWN:
            return 'Insediamento';
        case SettlementType.CITY:
            return 'Città';
        case SettlementType.CAPITAL:
            return 'Capitale';
        default:
            return 'Mistero!';
    }
}

function regionToString(id: Kingdoms) {
    switch (id) {
        case Kingdoms.EAST:
            return "Ducato dell'Est";
        case Kingdoms.WEST:
            return "Ducato dell'Ovest";
        case Kingdoms.NORTH:
            return 'Ducato del Nord';
        case Kingdoms.SOUTH:
            return 'Ducato del Sud';
        case Kingdoms.CAPITAL:
            return 'Terre del Re';
        default:
            return 'Terra di nessuno';
    }
}

export default function Slider() {
    const { fetchOne } = useSettlements();

    const [open, setOpen] = useState(false);
    const [currentSettlement, setCurrentSettlement] = useState<SettlementItem>();

    //callbacks
    const handleEvent = (townId: string) => {
        fetchOne(townId)
            .then((d: SettlementItem) => {
                setCurrentSettlement(d);
                setOpen(true);
            })
            .catch((e: Error) => console.error(e));
    };

    useEffect(() => {
        Emitter.on('openCitySlider', handleEvent);
        return () => {
            Emitter.off('openCitySlider', handleEvent);
        };
    }, []);

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog as='div' className='relative z-50' onClose={setOpen}>
                <Transition.Child
                    as={Fragment}
                    enter='ease-in-out duration-500'
                    enterFrom='opacity-0'
                    enterTo='opacity-100'
                    leave='ease-in-out duration-500'
                    leaveFrom='opacity-100'
                    leaveTo='opacity-0'>
                    <div className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity' />
                </Transition.Child>

                <div className='fixed inset-0 overflow-hidden'>
                    <div className='absolute inset-0 overflow-hidden'>
                        <div className='pointer-events-none fixed inset-y-0 right-0 flex max-w-full pl-10'>
                            <Transition.Child
                                as={Fragment}
                                enter='transform transition ease-in-out duration-500 sm:duration-700'
                                enterFrom='translate-x-full'
                                enterTo='translate-x-0'
                                leave='transform transition ease-in-out duration-500 sm:duration-700'
                                leaveFrom='translate-x-0'
                                leaveTo='translate-x-full'>
                                <Dialog.Panel className='pointer-events-auto relative w-96'>
                                    <Transition.Child
                                        as={Fragment}
                                        enter='ease-in-out duration-500'
                                        enterFrom='opacity-0'
                                        enterTo='opacity-100'
                                        leave='ease-in-out duration-500'
                                        leaveFrom='opacity-100'
                                        leaveTo='opacity-0'>
                                        <div className='absolute left-0 top-0 -ml-8 flex pr-2 pt-4 sm:-ml-10 sm:pr-4'>
                                            <button
                                                type='button'
                                                className='z-50 rounded-md text-gray-300 hover:text-white focus:outline-none focus:ring-2 focus:ring-white'
                                                onClick={() => setOpen(false)}>
                                                <span className='sr-only'>Close panel</span>
                                                <XMarkIcon className='h-6 w-6' aria-hidden='true' />
                                            </button>
                                        </div>
                                    </Transition.Child>
                                    <div className='h-full overflow-y-auto bg-secondary p-8'>
                                        <div className='space-y-6 pb-16'>
                                            <div>
                                                <div className='aspect-h-7 aspect-w-10 block max-h-64 w-full overflow-hidden rounded-lg'>
                                                    <img
                                                        src={
                                                            currentSettlement?.picture
                                                                ? `http://127.0.0.1:8090/api/files/${currentSettlement.collectionId}/${currentSettlement.id}/${currentSettlement.picture}`
                                                                : 'https://images.unsplash.com/photo-1582053433976-25c00369fc93?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=512&q=80'
                                                        }
                                                        alt=''
                                                        className='object-cover'
                                                    />
                                                </div>
                                                <div className='mt-4 flex items-start justify-between'>
                                                    <div>
                                                        <h2 className='text-base font-semibold leading-6 text-gray-900'>
                                                            <span className='sr-only'>
                                                                Details for{' '}
                                                            </span>
                                                            {currentSettlement?.name || ''}
                                                        </h2>
                                                        <p className='text-sm font-medium text-gray-500'>
                                                            {'<optional subtitle>'}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <h3 className='font-medium text-gray-900'>
                                                    Informazioni
                                                </h3>
                                                <dl className='mt-2 divide-y divide-gray-200 border-b border-t border-gray-200'>
                                                    <div className='flex justify-between py-3 text-sm font-medium'>
                                                        <dt className='text-gray-500'>Regione</dt>
                                                        <dd className='text-gray-900'>
                                                            {regionToString(
                                                                currentSettlement
                                                                    ? getKeyFromKingdomString(
                                                                          currentSettlement.region
                                                                      )
                                                                    : Kingdoms.CAPITAL
                                                            )}
                                                        </dd>
                                                    </div>
                                                    <div className='flex justify-between py-3 text-sm font-medium'>
                                                        <dt className='text-gray-500'>
                                                            Dimensione
                                                        </dt>
                                                        <dd className='text-gray-900'>
                                                            {settlementTypeToString(
                                                                currentSettlement
                                                                    ? getStringSettlementType(
                                                                          currentSettlement.settlementType
                                                                      )
                                                                    : SettlementType.TOWN
                                                            )}
                                                        </dd>
                                                    </div>
                                                </dl>
                                            </div>
                                            <div>
                                                <h3 className='font-medium text-gray-900'>
                                                    Descrizione
                                                </h3>
                                                <div className='mt-2 flex items-center justify-between'>
                                                    <div
                                                        className='text-sm italic text-gray-500'
                                                        dangerouslySetInnerHTML={{
                                                            __html: dompurify.sanitize(
                                                                currentSettlement?.description || ''
                                                            )
                                                        }}
                                                    />
                                                    <button
                                                        type='button'
                                                        className='-mr-2 flex h-8 w-8 flex-shrink-0 items-center justify-center rounded-full bg-white text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500'>
                                                        <PencilIcon
                                                            className='h-5 w-5'
                                                            aria-hidden='true'
                                                        />
                                                        <span className='sr-only'>
                                                            Add description
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div>
                                                <h3 className='font-medium text-gray-900'>
                                                    Personaggi
                                                </h3>
                                                <ul
                                                    role='list'
                                                    className='mt-2 divide-y divide-gray-200 border-b border-t border-gray-200'>
                                                    {currentSettlement?.expand.png?.map(
                                                        (character, index) => {
                                                            return (
                                                                <li
                                                                    key={'character' + index}
                                                                    className='flex items-center justify-between py-3'>
                                                                    <div className='flex items-center'>
                                                                        <img
                                                                            src={
                                                                                character.picture
                                                                                    ? `http://127.0.0.1:8090/api/files/${character.collectionId}/${character.id}/${character.picture}`
                                                                                    : 'https://images.unsplash.com/photo-1502685104226-ee32379fefbe?ixlib=rb-=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=3&w=1024&h=1024&q=80'
                                                                            }
                                                                            alt=''
                                                                            className='h-8 w-8 rounded-full'
                                                                        />
                                                                        <p className='ml-4 text-sm font-medium text-gray-900'>
                                                                            {character.name}
                                                                        </p>
                                                                    </div>
                                                                </li>
                                                            );
                                                        }
                                                    )}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </div>
                </div>
            </Dialog>
        </Transition.Root>
    );
}
