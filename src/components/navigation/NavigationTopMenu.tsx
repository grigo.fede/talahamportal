import { Menu, Transition } from '@headlessui/react';
import {
    BookOpenIcon,
    Cog6ToothIcon,
    GlobeAltIcon,
    IdentificationIcon,
    UserGroupIcon,
    UserIcon
} from '@heroicons/react/24/outline';
import PBClient from 'config/pbclient';
import { Fragment, PropsWithChildren, useMemo } from 'react';
import { Link } from 'react-router-dom';
import { useAppSelector } from 'store';
import { classNames } from 'utils';

function NavigationTopMenu(props: PropsWithChildren) {
    const user = useAppSelector(state => state.user);
    const isAuth: boolean = useMemo(() => user && PBClient.isAuth(), [user]);

    return (
        <Menu as='div' className='relative inline-block text-left'>
            <div>
                <Menu.Button>{props.children}</Menu.Button>
            </div>

            <Transition
                as={Fragment}
                enter='transition ease-out duration-100'
                enterFrom='transform opacity-0 scale-95'
                enterTo='transform opacity-100 scale-100'
                leave='transition ease-in duration-75'
                leaveFrom='transform opacity-100 scale-100'
                leaveTo='transform opacity-0 scale-95'>
                <Menu.Items className='absolute left-[110%] -top-[1px] z-10  origin-left rounded-md border  border-white  bg-[#100a24] shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none'>
                    <div className=' flex space-x-4 px-2'>
                        <Menu.Item>
                            {({ active }) => (
                                <Link
                                    to={'/'}
                                    className='block h-12 w-12 cursor-pointer rounded-md  hover:bg-slate-50/5'>
                                    <GlobeAltIcon className='h-full w-full p-2 text-white' />
                                </Link>
                            )}
                        </Menu.Item>
                        <Menu.Item>
                            {({ active }) => (
                                <div className='block h-12 w-12 cursor-pointer rounded-md  hover:bg-slate-50/5'>
                                    <UserGroupIcon className='h-full w-full p-2 text-white' />
                                </div>
                            )}
                        </Menu.Item>

                        <Menu.Item>
                            {({ active }) => (
                                <div className='block h-12 w-12 cursor-pointer rounded-md  hover:bg-slate-50/5'>
                                    <BookOpenIcon className='h-full w-full p-2 text-white' />
                                </div>
                            )}
                        </Menu.Item>

                        {isAuth && (
                            <Menu.Item>
                                {({ active }) => (
                                    <div className='block h-12 w-12 cursor-pointer rounded-md  hover:bg-slate-50/5'>
                                        <Cog6ToothIcon className='h-full w-full p-2 text-white' />
                                    </div>
                                )}
                            </Menu.Item>
                        )}

                        {isAuth && (
                            <Menu.Item>
                                {({ active }) => (
                                    <div className='block h-12 w-12 cursor-pointer rounded-md  hover:bg-slate-50/5'>
                                        <IdentificationIcon className='h-full w-full p-2 text-white' />
                                    </div>
                                )}
                            </Menu.Item>
                        )}
                        {/* <form method='POST' action='#'>
                            <Menu.Item>
                                {({ active }) => (
                                    <button
                                        type='submit'
                                        className={classNames(
                                            active ? 'bg-gray-100/10 text-gray-100' : 'text-white',
                                            'text-md block w-full px-4 py-2 text-left'
                                        )}></button>
                                )}
                            </Menu.Item>
                        </form> */}
                    </div>
                </Menu.Items>
            </Transition>
        </Menu>
    );
}

export default NavigationTopMenu;
