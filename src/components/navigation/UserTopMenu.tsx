import { Menu, Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/24/outline';
import { useMutation } from '@tanstack/react-query';
import PBClient from 'config/pbclient';
import { Fragment, PropsWithChildren, useEffect, useMemo } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'store';
import { logout } from 'store/slices/userSlice';
import { classNames } from 'utils';

function UserTopMenu(props: PropsWithChildren) {
    // hooks
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    // global state
    const { user } = useAppSelector(state => state.user);

    // memos
    const isAuth: boolean = useMemo(() => !!user && PBClient.isAuth(), [user]);

    useEffect(() => {
        console.log('detected change in is Auth', isAuth);
    }, [isAuth, user]);

    return (
        <Menu as='div' className='relative inline-block text-left'>
            <div>
                <Menu.Button>{props.children}</Menu.Button>
            </div>

            <Transition
                as={Fragment}
                enter='transition ease-out duration-100'
                enterFrom='transform opacity-0 scale-95'
                enterTo='transform opacity-100 scale-100'
                leave='transition ease-in duration-75'
                leaveFrom='transform opacity-100 scale-100'
                leaveTo='transform opacity-0 scale-95'>
                <Menu.Items className='absolute right-0 z-10  w-56 origin-top-right rounded-md border-2 border-white bg-[#9e8462] shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none'>
                    <div className='py-1'>
                        {isAuth && (
                            <Menu.Item>
                                {({ active }) => (
                                    <button
                                        onClick={() => {
                                            navigate('/profile', { replace: true });
                                        }}
                                        className={classNames(
                                            active ? 'bg-gray-100/10 text-gray-100' : 'text-white',
                                            'text-md block px-4 py-2'
                                        )}>
                                        Account settings
                                    </button>
                                )}
                            </Menu.Item>
                        )}
                        <Menu.Item>
                            {({ active }) => (
                                <a
                                    href='#'
                                    className={classNames(
                                        active ? 'bg-gray-100/10 text-gray-100' : 'text-white',
                                        'text-md block px-4 py-2'
                                    )}>
                                    About
                                </a>
                            )}
                        </Menu.Item>

                        {isAuth ? (
                            <Menu.Item>
                                {({ active }) => (
                                    <button
                                        onClick={() => {
                                            dispatch(logout());
                                            navigate('/', { replace: true });
                                        }}
                                        className={classNames(
                                            active ? 'bg-gray-100/10 text-gray-100' : 'text-white',
                                            'text-md block w-full px-4 py-2 text-left'
                                        )}>
                                        Sign out
                                    </button>
                                )}
                            </Menu.Item>
                        ) : (
                            <Menu.Item>
                                {({ active }) => (
                                    <Link
                                        to='/signin'
                                        className={classNames(
                                            active ? 'bg-gray-100/10 text-gray-100' : 'text-white',
                                            'text-md block w-full px-4 py-2 text-left'
                                        )}>
                                        Sign in
                                    </Link>
                                )}
                            </Menu.Item>
                        )}
                    </div>
                </Menu.Items>
            </Transition>
        </Menu>
    );
}

export default UserTopMenu;
