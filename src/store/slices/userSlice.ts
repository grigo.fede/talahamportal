// Redux
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import PBClient from 'config/pbclient';
import Cookie from 'cookie-universal';

// Types

const initialState: {
    user?: {
        name: string;
        surname: string;
        email: string;
        id: string;
        username: string;
        avatar: string;
    };
    role: string;
    avatar?: string;
} = {
    user: undefined,
    role: '',
    avatar: undefined
};

export const userSlice = createSlice({
    name: 'userSlice',
    initialState,
    reducers: {
        //set the current user credentials
        setUser: (
            state,
            action: PayloadAction<{
                user: {
                    name: string;
                    surname: string;
                    email: string;
                    id: string;
                    username: string;
                    avatar: string;
                };
                navigateCallback?: () => void;
            }>
        ) => {
            state = { ...state, user: action.payload.user };

            if (action.payload.navigateCallback) action.payload.navigateCallback();
            return state;
        },

        setAvatar: (state, action: PayloadAction<{ avatar: string }>) => {
            state = { ...state, avatar: action.payload.avatar };
            return state;
        },
        // set the user role
        setRole: (state, action: PayloadAction<{ role: string }>) => {
            state = { ...state, role: action.payload.role };
            return state;
        },

        //logout the current user
        logout: state => {
            console.log('logout');
            PBClient.getAuthStore().clear();
            const cookies = Cookie();
            cookies.remove('authcookie');
            // state = { ...state, ...initialState };
            return { ...state, ...initialState };
        }
    }
});

export const { setUser, setRole, setAvatar, logout } = userSlice.actions;

export default userSlice.reducer;
