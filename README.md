# Talahm Map

mappa interattiva per una campagna D&D

TODO:

-   [ ] completare la definizione dei nomi per gli insediamenti a Sud e Ovest
-   [ ] migliorare lo stile dei popup
-   [ ] valutare barra laterale con dettagli ed info sul landmark.
-   [ ] Aggiungere landMark naturali
-   [ ] valutare introduzione backend
-   [ ] valutare passaggio a Next/Remix/Astro
-   [] Hostare le tile online anzichè nel repo
